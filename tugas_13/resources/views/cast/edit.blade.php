@extends('layouts.master')
@section('title')
	Halaman Edit Cast
@endsection
@section('sub-title')
	halaman edit cast
@endsection
@section('content')

<form action="/cast{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" placeholder="Enter Name" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control" placeholder="Enter Age" name="umur" value="{{$cast->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea class="form-control" rows="3" name="bio">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
	